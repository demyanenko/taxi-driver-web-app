import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {Storage, history} from './storage';
import {ConnectedRouter} from 'react-router-redux';

import Main from "./components/Main";
import 'bootstrap/dist/css/bootstrap.css';
import 'react-notifications/lib/notifications.css';
require('jquery/dist/jquery');
require('popper.js/dist/umd/popper');
require('bootstrap/dist/js/bootstrap');

// Now you can dispatch navigation actions from anywhere!
// store.dispatch(push('/foo'))

export default ReactDOM.render(
    <Provider store={Storage}>
        <ConnectedRouter history={history}>
            <Main />
        </ConnectedRouter>
    </Provider>,
    document.getElementById('root')
)
