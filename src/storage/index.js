import { createStore, combineReducers, applyMiddleware } from 'redux';
import createHistory from 'history/createBrowserHistory';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import {composeWithDevTools} from 'redux-devtools-extension/logOnlyInProduction';

import {auth} from '../components/Home/reducers/auth';
import {profile} from '../components/Profile/reducers/profile';
import {proposal} from '../components/Proposal/reducers/proposal';
import {orders} from '../components/Orders/reducers/orders';
import {socket} from '../storage/socket';

const history = createHistory();

// Build the middleware for intercepting and dispatching navigation actions
const middleware = routerMiddleware(history);

const composeEnhancers = composeWithDevTools({});

// Add the reducer to your store on the `router` key
// Also apply our middleware for navigating
const Storage = createStore(
    combineReducers({
        router: routerReducer,
        auth, profile, socket, proposal, orders,
    }),
    composeEnhancers(applyMiddleware(middleware))
);

Storage.subscribe(() => {
    // console.log('subscribe', Redux.getState());
});

export {Storage, history};
