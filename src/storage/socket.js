import openSocket from 'socket.io-client';
import Emitter from '../sockets';

const initialState = {
    socket: null,
    emitter: null,
};

export const socket = (state = initialState, action) => {
    let payload = action.payload || {};

    switch (action.type) {

        case '@socket/CONNECT':
            if (state.socket === null && payload.token) {
                let socket = openSocket('http://127.0.0.1:1337/driver'),
                    emitter = new Emitter(socket, payload.token);
                return {
                    ...state,
                    socket,
                    emitter,
                };
            }
            return state;

        case '@socket/SEND_COORDINATES':
            state.emitter.sendCoordinates(payload.longitude, payload.latitude);
            return state;

        case '@socket/SUBSCRIBE_ON_AREAS':
            state.emitter.subscribeOnAreas();
            return state;

        case '@socket/UNSUBSCRIBE_FROM_AREAS':
            state.emitter.unSubscribeFromAreas();
            return state;

        default:
            return state;

    }

};