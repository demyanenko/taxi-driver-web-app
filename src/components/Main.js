import React, {Component, Fragment} from 'react';
import {Header, Routes} from './widgets';
import Cookie from 'react-cookies';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import Curl from '../libs/Curl';
import {connect} from 'react-redux';
import {push} from 'react-router-redux';
import Proposal from '../components/Proposal';

class Main extends Component {
    async componentWillMount() {
        let token = Cookie.load('token');
        if (token) {
            this.props.setToken(token);
            let response = await Curl.get('/user/profile');
            if (response.success === true) {
                this.props.setUser({
                    user: response.user,
                    car: response.car,
                    documents: response.documents || [],
                    token,
                });
                this.props.connectToSockets(token);
            } else {
                this.props.removeToken();
                this.props.setUser({});
                NotificationManager.warning(`${response.error.code}: ${response.error.message}`);
            }
            let checkStatus = await Curl.get('/user/status');
            if (checkStatus.success === true) {
                this.props.changeOnlineStatus({
                    online: checkStatus.isOnline,
                });
            } else {
                NotificationManager.warning(`${response.error.code}: ${response.error.message}`);
            }
        }
        if (!Object.keys(this.props.user).length) {
            this.props.redirectToHomePage();
        }
    }

    render() {
        return (
            <Fragment>
                <Header/>
                <div className="container pt-3">
                    <Proposal/>
                    <div className="content">
                        <Routes/>
                    </div>
                </div>
                <NotificationContainer/>
            </Fragment>
        );
    }
}

export default connect(state => {
    return {
        router: state.router,
        user: state.profile.user || {},
    };
}, dispatch => {
    return {
        setToken(token) {
            return dispatch({
                type: '@auth/SET_TOKEN',
                payload: {
                    token,
                },
            });
        },
        removeToken() {
            return dispatch({
                type: '@profile/REMOVE_TOKEN',
                payload: {},
            });
        },
        setUser(payload) {
            return dispatch({
                type: '@profile/SET_USER',
                payload,
            });
        },
        changeOnlineStatus(payload) {
            return dispatch({type: '@profile/SET_ONLINE_STATUS', payload});
        },
        connectToSockets(token) {
            return dispatch({
                type: '@socket/CONNECT',
                payload: {
                    token,
                },
            });
        },
        redirectToHomePage: () => {
            return dispatch(push('/'));
        },
    };
})(Main);