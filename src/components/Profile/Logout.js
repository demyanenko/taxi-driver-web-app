import React, {Component} from 'react';
import {connect} from 'react-redux';
import Curl from '../../libs/Curl';
import {Redirect} from 'react-router';

class Documents extends Component {

    async componentWillMount() {
        await Curl.post('/user/logout');
        this.props.logout();
    }

    render() {
        return <Redirect to="/"/>;
    }

}

export default connect(state => ({...state.profile, router: state.router}),
    dispatch => {
        return {
            logout: () => {
                return dispatch({type: '@profile/LOGOUT'}) &&
                    dispatch({type: '@auth/REMOVE_TOKEN'});
            },
        };
    })(Documents);