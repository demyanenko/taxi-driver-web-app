import React, { Component } from "react";
import {connect} from 'react-redux';

class PersonalInformation extends Component {

    render() {
        let invitationCode = '';
        if (!(
            this.props.user.name ||
            this.props.user.surname ||
            this.props.user.email
        )) {
            invitationCode =
                <div className="form-group">
                    <label htmlFor="phone" className="col-form-label">
                        Invitation code
                    </label>
                    <input type="text"
                           name="user[invitationCode]"
                           className="form-control"
                    />
                </div>;
        }
        let image = '';
        if (this.props.user.photo) {
            image = <img alt="Driver"
                         title="Driver"
                         src={this.props.user.photo}
                         style={{maxWidth: '100px', display: 'block'}} />
        }
        return (
            <div style={{display: this.props.step === 1 ? 'block' : 'none'}}>
                <div className="form-group">
                    <label htmlFor="phone" className="col-form-label">
                        Name
                    </label>
                    <input type="text"
                           name="user[name]"
                           className="form-control"
                           defaultValue={this.props.user.name}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="phone" className="col-form-label">
                        Surname
                    </label>
                    <input type="text"
                           name="user[surname]"
                           className="form-control"
                           defaultValue={this.props.user.surname}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="phone" className="col-form-label">
                        Email
                    </label>
                    <input type="text"
                           name="user[email]"
                           className="form-control"
                           defaultValue={this.props.user.email}
                    />
                </div>
                {invitationCode}
                <div className="form-group">
                    <label className="col-form-label">
                        Photo
                    </label>
                    {image}
                    <input type="file" className="form-control-file" name="user[photo]"/>
                </div>
                <div className="form-group">
                    <a
                        className="btn btn-primary"
                        onClick={this.nextStep.bind(this)}
                    >
                        Next step
                    </a>
                </div>
            </div>
        );
    }

    nextStep() {
        this.props.setStep({step: 2});
    }
}

export default connect(state => ({...state.profile}), dispatch => {
    return {
        setStep: (payload) => {
            return dispatch({type: "@profile/SET_STEP", payload})
        },
    };
})(PersonalInformation);