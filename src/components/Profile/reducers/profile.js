const initialState = {
    user: {},
    car: {},
    documents: [],
    step: 1,
    brands: [],
    models: [],
    years: [],
    colors: [],
    online: false,
    coordinates: {},
};

export const profile = (state = initialState, action) => {
    let payload = action.payload || {};

    switch (action.type) {

        case '@profile/LOGOUT':
            return {
                ...state,
                user: {},
                car: {},
                documents: [],
                step: 1,
            };

        case '@profile/SET_USER':
            return {
                ...state,
                user: payload.user,
                car: payload.car || {},
                documents: payload.documents || [],
            };

        case '@profile/SET_STEP':
            return {
                ...state,
                step: payload.step,
            };

        case '@profile/SET_BRANDS':
            return {
                ...state,
                brands: payload.brands,
            };

        case '@profile/SET_MODELS':
            return {
                ...state,
                models: payload.models,
            };

        case '@profile/SET_YEARS':
            return {
                ...state,
                years: payload.years,
            };

        case '@profile/SET_COLORS':
            return {
                ...state,
                colors: payload.colors,
            };

        case '@profile/SET_ONLINE_STATUS':
            return {
                ...state,
                online: payload.online,
            };

        case '@profile/CHANGE_COORDINATES':
            return {
                ...state,
                coordinates: payload,
            };

        default:
            return state;

    }

};