import React, { Component } from "react";
import {connect} from 'react-redux';
import Curl from '../../libs/Curl';
import {NotificationManager} from 'react-notifications';

class CarInformation extends Component {

    async componentWillMount() {
        if (!this.props.colors.length) {
            this.updateColors();
        }
        if (!this.props.brands.length) {
            await this.updateBrands();
            if (this.props.car.model && this.props.car.model.id && !this.props.models.length) {
                await this.updateModels();
                if (this.props.car.year && !this.props.years.length) {
                    await this.updateYears();
                }
            }
        }
    }

    render() {
        let image = '';
        if (this.props.car.photo) {
            image = <img alt="Car"
                         title="Car"
                         src={this.props.car.photo}
                         style={{maxWidth: '100px', display: 'block'}} />
        }
        return (
            <div style={{display: this.props.step === 2 ? 'block' : 'none'}}>
                <div className="form-group">
                    <label className="col-form-label">
                        Brand
                    </label>
                    <select className="form-control"
                            name="car[brandId]"
                            ref="brandSelect"
                            defaultValue={this.props.car.brand ? this.props.car.brand.id : ''}
                            onChange={this.updateModels.bind(this)}
                    >
                        <option value="" key="brands">&mdash;</option>
                        {this.props.brands.map((item, index) => (
                            <option value={item.id} key={`brand-${item.id}`}>{item.name}</option>
                        ))}
                    </select>
                </div>
                <div className="form-group">
                    <label className="col-form-label">
                        Model
                    </label>
                    <select className="form-control"
                            name="car[modelId]"
                            ref="modelSelect"
                            defaultValue={this.props.car.model ? this.props.car.model.id : ''}
                            onChange={this.updateYears.bind(this)}
                    >
                        <option value="" key="models">&mdash;</option>
                        {this.props.models.map((item, index) => (
                            <option value={item.id} key={`model-${item.id}`}>{item.name}</option>
                        ))}
                    </select>
                </div>
                <div className="form-group">
                    <label className="col-form-label">
                        Year
                    </label>
                    <select
                        className="form-control"
                        ref="yearSelect"
                        name="car[year]"
                        defaultValue={this.props.car.year || ''}
                    >
                        <option value="" key="years">&mdash;</option>
                        {this.props.years.map((item, index) => (
                            <option value={item} key={`year-${item}`}>{item}</option>
                        ))}
                    </select>
                </div>
                <div className="form-group">
                    <label className="col-form-label">
                        Color
                    </label>
                    <select
                        className="form-control"
                        ref="colorSelect"
                        name="car[colorId]"
                        defaultValue={this.props.car.color ? this.props.car.color.id : ''}
                    >
                        <option value="" key="colors">&mdash;</option>
                        {this.props.colors.map((item, index) => (
                            <option value={item.id} key={`color-${item.id}`}>{item.name}</option>
                        ))}
                    </select>
                </div>
                <div className="form-group">
                    <label className="col-form-label">
                        Number
                    </label>
                    <input type="text"
                           className="form-control"
                           defaultValue={this.props.car.number}
                           name="car[number]"
                    />
                </div>
                <div className="form-group">
                    <label className="col-form-label">
                        Photo
                    </label>
                    {image}
                    <input type="file" className="form-control-file" name="car[photo]"/>
                </div>
                <div className="form-group">
                    <a
                        className="btn btn-info"
                        onClick={this.prevStep.bind(this)}
                    >
                        Previous step
                    </a>
                    <a
                        className="btn btn-primary"
                        onClick={this.nextStep.bind(this)}
                    >
                        Next step
                    </a>
                </div>
            </div>
        );
    }

    async nextStep() {
        this.props.setStep({step: 3});
    }

    async prevStep() {
        this.props.setStep({step: 1});
    }

    async updateBrands() {
        if (this.refs.modelSelect) {
            this.refs.modelSelect.value = '';
        }
        this.props.setModels({
            models: [],
        });
        let response = await Curl.get('/car/brands');
        if (response.success === true) {
            this.props.setBrands({
                brands: response.brands,
            });
            this.refs.brandSelect.value = this.props.car.brand ? this.props.car.brand.id : '';
        } else {
            this.props.setBrands({
                brands: [],
            });
            NotificationManager.warning(`${response.error.code}: ${response.error.message}`);
        }
    }

    async updateColors() {
        let response = await Curl.get('/car/colors');
        if (response.success === true) {
            this.props.setColors({
                colors: response.colors,
            });
            this.refs.colorSelect.value = this.props.car.color ? this.props.car.color.id : '';
        } else {
            this.props.setColors({
                colors: [],
            });
            NotificationManager.warning(`${response.error.code}: ${response.error.message}`);
        }
    }

    async updateModels() {
        this.props.setYears({
            years: [],
        });
        if (!this.refs.brandSelect.value) {
            this.props.setModels({
                models: [],
            });
            return;
        }
        let response = await Curl.get(`/car/models/${this.refs.brandSelect.value}`);
        if (response.success === true) {
            this.props.setModels({
                models: response.models,
            });
            this.refs.modelSelect.value = this.props.car.model ? this.props.car.model.id : '';
        } else {
            this.props.setModels({
                models: [],
            });
            NotificationManager.warning(`${response.error.code}: ${response.error.message}`);
        }
    }

    async updateYears() {
        if (!this.refs.modelSelect.value) {
            this.props.setYears({
                years: [],
            });
            return;
        }
        let response = await Curl.get(`/car/years/${this.refs.modelSelect.value}`);
        if (response.success === true) {
            this.props.setYears({
                years: response.years,
            });
            this.refs.yearSelect.value = this.props.car.year;
        } else {
            this.props.setYears({
                years: [],
            });
            NotificationManager.warning(`${response.error.code}: ${response.error.message}`);
        }
    }
}

export default connect(state => ({...state.profile}), dispatch => {
    return {
        setStep: (payload) => {
            return dispatch({type: "@profile/SET_STEP", payload})
        },
        setBrands: (payload) => {
            return dispatch({type: "@profile/SET_BRANDS", payload})
        },
        setModels: (payload) => {
            return dispatch({type: "@profile/SET_MODELS", payload})
        },
        setYears: (payload) => {
            return dispatch({type: "@profile/SET_YEARS", payload})
        },
        setColors: (payload) => {
            return dispatch({type: "@profile/SET_COLORS", payload})
        },
    };
})(CarInformation);