import React, { Component } from "react";
import {connect} from 'react-redux';
import PersonalInformation from './PersonalInformation';
import CarInformation from './CarInformation';
import Documents from './Documents';
import Curl from '../../libs/Curl';
import {NotificationManager} from 'react-notifications';
import {push} from 'react-router-redux';

class Profile extends Component {

    render() {
        if (Object.keys(this.props.user).length) {
            return (
                <form onSubmit={this.save.bind(this)}>
                    <PersonalInformation/>
                    <CarInformation/>
                    <Documents/>
                </form>
            );
        }
        return null;
    }

    async save(event) {
        event.preventDefault();
        let data = new FormData(), documents = [];
        for (let element of event.target) {
            if (element.type === 'submit' || element.value === '') {
                continue;
            }
            if (element.type === 'file') {
                if (element.name) {
                    data.append(element.name, element.files[0]);
                } else {
                    documents.push(element.files[0]);
                }
            } else {
                data.append(element.name, element.value);
            }
        }
        let response = await Curl.post('/user/registration', data);
        if (response.success === false) {
            NotificationManager.warning(`${response.error.code}: ${response.error.message}`);
            return;
        }
        for (let index in documents) {
            let data = new FormData(),
                id = parseInt(index, 0) + 1;
            data.append('id', id);
            data.append('photo', documents[index]);
            let response = await Curl.post('/user/load_document', data);
            if (response.success === false) {
                NotificationManager.warning(`Document ${id} - ${response.error.code}: ${response.error.message}`);
            }
        }
        this.props.firstStep();
        NotificationManager.success(`Data storing succeed!`);
        this.updateUser();
        this.props.history.push("/");
    }

    async updateUser() {
        let response = await Curl.get('/user/profile');
        if (response.success === true) {
            this.props.setUser({
                user: response.user || {},
                car: response.car || {},
                documents: response.documents || [],
            });
        } else {
            NotificationManager.warning(`${response.error.code}: ${response.error.message}`);
        }
    }

}

export default connect(state => ({...state.profile, router: state.router}), dispatch => ({
    firstStep: () => {
        return dispatch({type: "@profile/SET_STEP", payload: {step: 1}});
    },
    setUser: (payload) => {
        return dispatch({type: '@profile/SET_USER', payload});
    },
    redirectToHomePage: () => {
        return dispatch(push('/'));
    },
}))(Profile);