import React, { Component } from "react";
import {connect} from 'react-redux';
import Curl from '../../libs/Curl';

class Documents extends Component {

    constructor() {
        super();
        this.requiredDocumentsIds = [];
    }

    async componentWillMount() {
        let response = await Curl.get('/user/required_documents');
        if (response.success === false) {
            this.props.setStep({step: 2});
        } else {
            this.requiredDocumentsIds = response.necessaryDocumentsId;
        }
    }

    render() {
        return (
            <div style={{display: this.props.step === 3 ? 'block' : 'none'}}>
                <div className="form-group">
                    <label className="control-label">Водительское удостоверение, сторона с фотографией {
                        this.requiredDocumentsIds.indexOf(1) >= 0 ? '*' : ''
                    }</label>
                    {this.props.documents.map(document => {
                        if (document.id === 1) {
                            return <a href={document.url}
                                      target="_blank"
                                      key={document.id}
                                      style={{display: 'block'}}>Загружен...</a>
                        }
                        return '';
                    })}
                    <input type="file" className="form-control-file"/>
                </div>
                <div className="form-group">
                    <label className="control-label">Водительское удостоверение, сторона без фотографии {
                        this.requiredDocumentsIds.indexOf(2) >= 0 ? '*' : ''
                    }</label>
                    {this.props.documents.map(document => {
                        if (document.id === 2) {
                            return <a href={document.url}
                                      target="_blank"
                                      key={document.id}
                                      style={{display: 'block'}}>Загружен...</a>
                        }
                        return '';
                    })}
                    <input type="file" className="form-control-file"/>
                </div>
                <div className="form-group">
                    <label className="control-label">Тех.паспорт автомобиля, сторона с маркой авто {
                        this.requiredDocumentsIds.indexOf(3) >= 0 ? '*' : ''
                    }</label>
                    {this.props.documents.map(document => {
                        if (document.id === 3) {
                            return <a href={document.url}
                                      target="_blank"
                                      key={document.id}
                                      style={{display: 'block'}}>Загружен...</a>
                        }
                        return '';
                    })}
                    <input type="file" className="form-control-file"/>
                </div>
                <div className="form-group">
                    <label className="control-label">Тех.паспорт автомобиля, сторона с годом выпуска {
                        this.requiredDocumentsIds.indexOf(4) >= 0 ? '*' : ''
                    }</label>
                    {this.props.documents.map(document => {
                        if (document.id === 4) {
                            return <a href={document.url}
                                      target="_blank"
                                      key={document.id}
                                      style={{display: 'block'}}>Загружен...</a>
                        }
                        return '';
                    })}
                    <input type="file" className="form-control-file"/>
                </div>
                <div className="form-group">
                    <a
                        className="btn btn-info"
                        onClick={this.prevStep.bind(this)}
                    >
                        Previous step
                    </a>
                    <button
                        className="btn btn-primary"
                    >
                        Save data
                    </button>
                </div>
            </div>
        );
    }

    prevStep() {
        this.props.setStep({step: 2});
    }

}

export default connect(state => ({...state.profile}), dispatch => {
    return {
        setStep: (payload) => {
            return dispatch({type: "@profile/SET_STEP", payload})
        },
    };
})(Documents);