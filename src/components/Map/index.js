import {compose, withProps} from 'recompose';
import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {withScriptjs, withGoogleMap, GoogleMap} from 'react-google-maps';
import MapMarker from './Marker';

class Map extends Component {

    render() {
        const MapComponent = compose(
            withProps({
                googleMapURL: 'https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places',
                loadingElement: <div style={{height: `100%`}}/>,
                containerElement: <div style={{height: `400px`}}/>,
                mapElement: <div style={{height: `100%`}}/>,
            }),
            withScriptjs,
            withGoogleMap,
        )((props) =>
            <GoogleMap defaultZoom={12}
                       defaultCenter={{
                           lat: 46.62561234315254,
                           lng: 32.61181714183272,
                       }}
                       onClick={this.handleClickOnMap.bind(this)}
            >
                <MapMarker/>
            </GoogleMap>,
        );
        return (
            <Fragment>
                <MapComponent/>
                <div>
                    <button onClick={this.subscribe.bind(this)}>Subscribe</button>
                    <button onClick={this.unsubscribe.bind(this)}>Unsubscribe</button>
                </div>
            </Fragment>
        );
    }

    handleClickOnMap(event) {
        this.props.coordinates({
            longitude: event.latLng.lng(),
            latitude: event.latLng.lat(),
        });
    }

    subscribe() {
        this.props.subscribe();
    }

    unsubscribe() {
        this.props.unsubscribe();
    }
}

export default connect(state => ({
    router: state.router,
}), dispatch => {
    return {
        coordinates: (payload) => {
            return dispatch({type: '@socket/SEND_COORDINATES', payload}) &&
                dispatch({type: '@profile/CHANGE_COORDINATES', payload});
        },
        subscribe: (payload) => {
            return dispatch({type: '@socket/SUBSCRIBE_ON_AREAS', payload});
        },
        unsubscribe: (payload) => {
            return dispatch({type: '@socket/UNSUBSCRIBE_FROM_AREAS', payload});
        },
    };
})(Map);