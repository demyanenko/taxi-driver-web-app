import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Marker} from 'react-google-maps';

class MapMarker extends Component {

    render() {
        if (this.props.coordinates &&
            Object.keys(this.props.coordinates).length) {
            return (
                <Marker
                    onClick={this.onMarkerClick}
                    name={'Current location'}
                    position={{
                        lat: this.props.coordinates.latitude,
                        lng: this.props.coordinates.longitude,
                    }}
                />
            );
        }
        return null;
    }
}

export default connect(state => ({
    coordinates: state.profile.coordinates,
}), dispatch => {
    return {};
})(MapMarker);