import React, { Component } from "react";
import {connect} from 'react-redux';
import Curl from '../../libs/CurlToNode';

class Proposal extends Component {

    render() {
        if (!Object.keys(this.props.order).length) {
            return null;
        }
        let userInfo;
        if (this.props.order.passenger) {
            userInfo = `${this.props.order.passenger.name} (${this.props.order.passenger.phone})`;
        } else {
            userInfo = `${this.props.order.user.name} (${this.props.order.user.phone})`;
        }
        return (
            <div className="alert alert-warning alert-dismissible fade show mt-4" role="alert">
                <h4 className="alert-heading">New order #{this.props.order.id}</h4>
                <p>Passenger information: {userInfo}</p>
                <p>Distance to passenger: {this.props.order.distance} km</p>
                <p>Calculated path: {this.props.order.calculatedPath} km</p>
                <p>Coefficient: {this.props.order.coefficient}</p>
                <footer>
                    <button onClick={this.accept.bind(this)}>Accept</button>
                    <button onClick={this.skip.bind(this)}>Skip</button>
                </footer>
            </div>
        );
    }

    async accept() {
        await Curl.post(`/user/order/accept/${this.props.order.id}`, {
            eventTime: new Date().getTime(),
            longitude: this.props.coordinates ? this.props.coordinates.longitude : null,
            latitude: this.props.coordinates ? this.props.coordinates.latitude : null,
        });
        this.props.remove();
    }

    async skip() {
        await Curl.post(`/user/order/skip/${this.props.order.id}`, {
            eventTime: new Date().getTime(),
        });
        this.props.remove();
    }

}

export default connect(state => ({...state.proposal, ...state.profile, router: state.router}), dispatch => ({
    remove: () => {
        return dispatch({type: '@proposal/REMOVE'});
    },
}))(Proposal);