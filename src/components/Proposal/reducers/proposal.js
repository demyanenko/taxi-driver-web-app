const initialState = {
    order: {},
    formula: {},
};

export const proposal = (state = initialState, action) => {
    let payload = action.payload || {};

    switch (action.type) {

        case '@proposal/RECEIVE':
            return {
                ...state,
                order: payload.order,
                formula: payload.formula,
            };

        case '@proposal/REMOVE':
            return {
                ...state,
                order: {},
                formula: {},
            };

        default:
            return state;

    }

};