import React, {Component} from 'react';
import {connect} from 'react-redux';
import {NavLink} from 'react-router-dom';
import {NotificationManager} from 'react-notifications';
import Curl from '../../libs/Curl';

class Header extends Component {

    routes = [];

    render() {
        let checkbox = null;
        if (Object.keys(this.props.user).length) {
            checkbox = (
                <div className="my-2 my-lg-0">
                    <input
                        type="checkbox"
                        checked={!!this.props.online}
                        className="online-trigger"
                        onClick={this.changeOnlineStatus.bind(this)}
                    />
                </div>
            );
        }
        this.fillRoutes();
        return (
            <nav className="navbar navbar-expand-md navbar-light bg-faded bg-light">
                <ul className="navbar-nav mr-auto">
                    {this.routes.map(route => route)}
                </ul>
                {checkbox}
            </nav>
        );
    }

    fillRoutes() {
        this.routes = [
            <li className="nav-item" key="home">
                <NavLink exact to="/" className="nav-link">Home</NavLink>
            </li>
        ];
        if (this.props.token) {
            this.routes = [
                ...this.routes,
                <li className="nav-item" key="profile">
                    <NavLink to="/profile" className="nav-link" activeClassName="active">Profile</NavLink>
                </li>,
                <li className="nav-item" key="map">
                    <NavLink to="/map" className="nav-link" activeClassName="active">Map</NavLink>
                </li>,
                <li className="nav-item" key="current-orders">
                    <NavLink to="/current-orders" className="nav-link" activeClassName="active">Current orders</NavLink>
                </li>,
                <li className="nav-item" key="logout">
                    <NavLink to="/logout" className="nav-link" activeClassName="active">Log out</NavLink>
                </li>,
            ];
        }
    }

    async changeOnlineStatus() {
        let response = await Curl.post('/user/status', {
            isOnline: !this.props.online,
        });
        if (response.success === false) {
            NotificationManager.warning(`${response.error.code}: ${response.error.message}`);
            return;
        }
        this.props.changeOnlineStatus({
            online: !this.props.online,
        });
    }

}

export default connect((state) => {
    return {
        user: state.profile.user || {},
        online: state.profile.online,
        router: state.router,
        token: state.auth.token,
    };
}, dispatch => {
    return {
        changeOnlineStatus: (payload) => {
            return dispatch({type: '@profile/SET_ONLINE_STATUS', payload});
        },
    };
})(Header);