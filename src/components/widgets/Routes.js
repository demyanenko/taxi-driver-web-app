import React, {Component} from 'react';
import {Route, Switch} from 'react-router';
import Home from '../Home';
import Profile from '../Profile';
import Map from '../Map';
import Logout from '../Profile/Logout';
import CurrentOrders from '../Orders/Current';

export default class Routes extends Component {

    render() {
        return (
            <Switch>
                <Route exact path="/" component={Home}/>
                <Route path="/profile" component={Profile}/>
                <Route path="/map" component={Map}/>
                <Route path="/current-orders" component={CurrentOrders}/>
                <Route path="/logout" component={Logout}/>
            </Switch>
        );
    }

}