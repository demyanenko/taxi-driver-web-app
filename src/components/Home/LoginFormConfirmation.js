import React, { Component } from "react";
import {connect} from 'react-redux';
import Curl from '../../libs/Curl';
import {NotificationManager} from 'react-notifications';

class LoginFormConfirmation extends Component {

    render() {
        return (
            <div role="form">
                <div className="form-group">
                    <label htmlFor="code" className="col-form-label">
                        Code
                    </label>
                    <input type="text"
                           className="form-control"
                           ref={(input) => {this.codeInput = input}}
                    />
                </div>
                <div className="form-group">
                    <button
                        className="btn btn-primary"
                        onClick={this.sendCode.bind(this)}
                    >
                        Log in
                    </button>
                </div>
            </div>
        );
    }

    async sendCode() {
        let response = await Curl.post('/user/confirm_code', {
            phone: this.props.auth.submittedPhone,
            code: this.codeInput.value,
        });
        if (response.success === true) {
            this.props.setPhone({
                phone: null,
            });
            this.props.setToken({
                token: response.authorizationToken,
            });
            this.updateUser();
            NotificationManager.success(`You are logged in!`);
        } else {
            this.props.setToken({
                token: null,
            });
            NotificationManager.warning(`${response.error.code}: ${response.error.message}`);
        }
    }

    async updateUser() {
        let response = await Curl.get('/user/profile');
        if (response.success === true) {
            this.props.setUser({
                user: response.user,
                car: response.car,
                documents: response.documents || [],
            });
        } else {
            NotificationManager.warning(`${response.error.code}: ${response.error.message}`);
        }
    }
}

export default connect(state => ({
    auth: state.auth,
}), dispatch => {
    return {
        setPhone: (payload) => {
            return dispatch({type: "@auth/SEND_PHONE", payload})
        },
        setToken: (payload) => {
            return dispatch({type: "@auth/SET_TOKEN", payload}) &&
                   dispatch({type: "@socket/CONNECT", payload});
        },
        setUser: (payload) => {
            return dispatch({type: '@profile/SET_USER', payload});
        },
    };
})(LoginFormConfirmation);