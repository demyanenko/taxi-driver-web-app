import Cookie from 'react-cookies';

const initialState = {
    token: null,
    submittedPhone: null,
};

export const auth = (state = initialState, action) => {
    let payload = action.payload || {};

    switch (action.type) {
        case '@auth/SEND_PHONE':
            return {
                ...state,
                submittedPhone: payload.phone,
            };

        case '@auth/SET_TOKEN':
            Cookie.save('token', payload.token, {
                path: '/',
            });
            return {
                ...state,
                token: payload.token,
            };

        case '@auth/REMOVE_TOKEN':
            if (Cookie.load('token')) {
                Cookie.remove('token');
            }
            return {
                ...state,
                token: null,
            };

        default:
            return state;
    }

};