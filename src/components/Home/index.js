import React, { Component, Fragment } from "react";
import {connect} from 'react-redux';
import LoginForm from './LoginForm';
import LoginFormConfirmation from './LoginFormConfirmation';
import Dashboard from './Dashboard';

class Home extends Component {
    render() {
        let content;
        if(this.props.submittedPhone !== null) {
            content = <LoginFormConfirmation/>;
        } else if (this.props.token === null) {
            content = <LoginForm/>;
        } else {
            content = <Dashboard/>;
        }
        return (
            <Fragment>
                {content}
            </Fragment>
        );
    }
}

export default connect(state => ({
    token: state.auth.token,
    submittedPhone: state.auth.submittedPhone,
    router: state.router,
}), dispatch => ({}))(Home);