import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';

class Dashboard extends Component {

    render() {
        let content;
        if (!this.props.user || !Object.keys(this.props.user).length) {
            content = 'Loading...';
        } else {
            content = `Hello, ${this.props.user.name || 'Unknown'} [ID ${this.props.user.id}]!`;
        }
        return (
            <Fragment>
                {content}
            </Fragment>
        );
    }

}

export default connect(state => ({user: state.profile.user, router: state.router}), dispatch => {
    return {};
})(Dashboard);