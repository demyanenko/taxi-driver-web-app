import React, { Component } from "react";
import {connect} from 'react-redux';
import Curl from '../../libs/Curl';
import {NotificationManager} from 'react-notifications';

class LoginForm extends Component {

    render() {
        return (
            <div role="form">
                <div className="form-group">
                    <label htmlFor="phone" className="col-form-label">
                        Phone number
                    </label>
                    <input type="text"
                           className="form-control"
                           ref={(input) => {this.phoneInput = input}}
                    />
                </div>
                <div className="form-group">
                    <button
                        className="btn btn-primary"
                        onClick={this.sendPhone.bind(this)}
                    >
                        Log in
                    </button>
                </div>
            </div>
        );
    }

    async sendPhone() {
        let response = await Curl.post('/user/phone', {
            phone: this.phoneInput.value,
            language: 0,
        });
        if (response.success === true) {
            this.props.setPhone({
                phone: this.phoneInput.value,
            });
        } else {
            this.props.setPhone({
                phone: null,
            });
            NotificationManager.warning(`${response.error.code}: ${response.error.message}`);
        }
    }
}

export default connect(state => ({auth: state.auth}), dispatch => {
    return {
        setPhone: (payload) => {
            return dispatch({type: "@auth/SEND_PHONE", payload})
        },
    };
})(LoginForm);