import React, { Component } from "react";
import {connect} from 'react-redux';
import Curl from '../../libs/CurlToNode';
import {NotificationManager} from 'react-notifications';

class CurrentOrder extends Component {

    render() {
        let buttons = [];
        if (this.props.order.status === 2) {
            buttons.push(<button key="arrived" onClick={this.arrived.bind(this, this.props.order)}>Arrived</button>);
        }
        if (this.props.order.status === 3) {
            buttons.push(<button key="onMyWay" onClick={this.onMyWay.bind(this, this.props.order)}>On my way</button>);
        }
        if (this.props.order.status === 4) {
            buttons.push(<button key="totalCost" onClick={this.totalCost.bind(this, this.props.order)}>Total cost</button>);
        }
        if (this.props.order.status < 5) {
            buttons.push(<button key="refuse" onClick={this.refuse.bind(this, this.props.order)}>Refuse</button>);
        }
        if (this.props.order.status === 7) {
            buttons.push(<button key="finish" onClick={this.finish.bind(this, this.props.order)}>Finish</button>);
            buttons.push(<button key="unpaid" onClick={this.unpaid.bind(this, this.props.order)}>Unpaid</button>);
        }
        return (
            <tr key={`current-order-${this.props.order.id}`}>
                <td>#{this.props.order.id}</td>
                <td>
                    {
                        this.props.order.passenger ?
                        `${this.props.order.passenger.name} (${this.props.order.passenger.phone})` :
                        `${this.props.order.user.name} (${this.props.order.user.phone})`
                    }
                </td>
                <td>{this.props.order.distance} km</td>
                <td>{this.statusText(this.props.order.status)}</td>
                <td>{this.props.order.payment.estimatedCost} grn</td>
                <td>{this.props.order.calculatedPath} km</td>
                <td>{buttons.map(button => button)}</td>
            </tr>
        );
    }

    async refuse(order) {
        let response = await Curl.post(`/user/order/refuse/${order.id}`, {
            reasonId: 1,
            comment: "This is my mistake",
            status: order.status,
            factCost: order.payment.estimatedCost,
            actualCost: order.payment.estimatedCost,
            fact_time: 592000,
            fact_path: order.calculatedPath,
            eventTime: new Date().getTime(),
            route: order.route || [],
        });
        if (response.success) {
            this.props.remove({orderId: order.id});
        } else {
            NotificationManager.warning(`${response.error.code}: ${response.error.message}`);
        }
    }

    async arrived(order) {
        let response = await Curl.post(`/user/order/arrived/${order.id}`, {
            eventTime: new Date().getTime(),
            longitude: this.props.longitude || null,
            latitude: this.props.latitude || null,
        });
        if (response.success) {
            this.props.changeStatus({orderId: order.id, status: 3});
        } else {
            NotificationManager.warning(`${response.error.code}: ${response.error.message}`);
        }
    }

    async onMyWay(order) {
        let response = await Curl.post(`/user/order/in_way/${order.id}`, {
            eventTime: new Date().getTime(),
            longitude: this.props.longitude || null,
            latitude: this.props.latitude || null,
        });
        if (response.success) {
            this.props.changeStatus({orderId: order.id, status: 4});
        } else {
            NotificationManager.warning(`${response.error.code}: ${response.error.message}`);
        }
    }

    async totalCost(order) {
        let response = await Curl.post(`/user/order/total_cost/${order.id}`, {
            factCost: order.payment.estimatedCost,
            actualCost: order.payment.estimatedCost,
            fact_time: 592000,
            fact_path: order.calculatedPath,
            eventTime: new Date().getTime(),
            longitude: this.props.longitude || null,
            latitude: this.props.latitude || null,
            route: order.route || [],
            changedParams: 2,
        });
        if (response.success) {
            this.props.changeStatus({orderId: order.id, status: 7});
        } else {
            NotificationManager.warning(`${response.error.code}: ${response.error.message}`);
        }
    }

    async finish(order) {
        let response = await Curl.post(`/user/order/finish/${order.id}`, {
            eventTime: new Date().getTime(),
            longitude: this.props.longitude || null,
            latitude: this.props.latitude || null,
        });
        if (response.success) {
            this.props.remove({orderId: order.id});
        } else {
            NotificationManager.warning(`${response.error.code}: ${response.error.message}`);
        }
    }

    async unpaid(order) {
        let response = await Curl.post(`/user/order/unpaid/${order.id}`, {
            eventTime: new Date().getTime(),
            longitude: this.props.longitude || null,
            latitude: this.props.latitude || null,
            reasonId: 2,
            comment: ':('
        });
        if (response.success) {
            this.props.remove({orderId: order.id});
        } else {
            NotificationManager.warning(`${response.error.code}: ${response.error.message}`);
        }
    }

    statusText(status) {
        switch (status) {
            case 0:
                return 'New';
            case 1:
                return 'Searching...';
            case 2:
                return 'Accepted';
            case 3:
                return 'Arrived';
            case 4:
                return 'On my way';
            case 5:
                return 'Completed';
            case 6:
                return 'Refused';
            case 7:
                return 'Waiting for payment';
            default:
                return 'Unknown';
        }
    }

}

export default connect(state => ({...state.orders, ...state.profile.coordinates, router: state.router}), dispatch => ({
    remove: (payload) => {
        return dispatch({type: '@orders/REMOVE_CURRENT_ORDER', payload});
    },
    changeStatus: (payload) => {
        return dispatch({type: '@orders/CHANGE_STATUS_TO_CURRENT_ORDER', payload});
    },
}))(CurrentOrder);