import React, { Component } from "react";
import {connect} from 'react-redux';
import Curl from '../../libs/CurlToNode';
import CurrentOrder from './CurrentOrder';

class Current extends Component {

    async componentWillMount() {
        let orders = await Curl.get('/user/current_orders');
        this.props.setCurrentOrdersList(orders);
    }

    render() {
        if (!this.props.currentOrders || !this.props.currentOrders.length) {
            return (
                <div>
                    No orders in the moment
                </div>
            );
        }
        return (
            <table className="table table-bordered">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Passenger</th>
                    <th>Distance</th>
                    <th>Status</th>
                    <th>Cost</th>
                    <th>Calculated path</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                {this.props.currentOrders.map((order) => {
                    return <CurrentOrder key={order.order.id} order={order.order} />
                })}
                </tbody>
            </table>
        );
    }

}

export default connect(state => ({...state.orders, ...state.profile.coordinates, router: state.router}), dispatch => ({
    setCurrentOrdersList: (payload) => {
        return dispatch({type: '@orders/SET_CURRENT_ORDERS_LIST', payload});
    },
    remove: (payload) => {
        return dispatch({type: '@orders/REMOVE_CURRENT_ORDER', payload});
    },
    changeStatus: (payload) => {
        return dispatch({type: '@orders/CHANGE_STATUS_TO_CURRENT_ORDER', payload});
    },
}))(Current);