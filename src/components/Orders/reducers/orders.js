const initialState = {
    currentOrders: [],
};

export const orders = (state = initialState, action) => {
    let payload = action.payload || {}, newOrdersList;

    switch (action.type) {

        case '@orders/SET_CURRENT_ORDERS_LIST':
            return {
                ...state,
                currentOrders: payload.orders,
            };

        case '@orders/REMOVE_CURRENT_ORDER':
            newOrdersList = [];
            for (let index in state.currentOrders) {
                let instance = state.currentOrders[index];
                if (instance.order.id !== payload.orderId) {
                    newOrdersList.push(instance);
                }
            }
            return {
                ...state,
                currentOrders: newOrdersList,
            };

        case '@orders/CHANGE_STATUS_TO_CURRENT_ORDER':
            newOrdersList = [];
            for (let index in state.currentOrders) {
                let instance = state.currentOrders[index];
                if (instance.order.id === parseInt(payload.orderId)) {
                    instance.order.status = payload.status;
                }
                newOrdersList.push(instance);
            }
            return {
                ...state,
                currentOrders: newOrdersList,
            };

        default:
            return state;

    }

};