import cookie from 'react-cookies';
import axios from 'axios';

export default class Curl {
    
    static baseUrl = 'http://127.0.0.1:8000/driver/v1';
    
    static defaultHeaders = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    };

    static getHeaders() {
        let headers = Curl.defaultHeaders;
        const token = cookie.load('token');
        if (token) {
            headers['Authorization'] = `Bearer ${token}`;
        }
        return headers;
    }

    static get(url) {
        return new Promise((resolve, reject) => {
            axios({
                method: 'get',
                url: Curl.baseUrl + url,
                headers: Curl.getHeaders(),
            })
            .then((resp) => {
                resolve(resp.data);
            })
            .catch(error => {
                resolve({
                    success: false,
                    error: {
                        code: 500,
                        message: error,
                    },
                });
            });
        });
    }

    static post(url, data) {
        return new Promise((resolve, reject) => {
            axios({
                method: 'post',
                url: Curl.baseUrl + url,
                headers: Curl.getHeaders(),
                data,
            })
            .then((resp) => {
                resolve(resp.data);
            })
            .catch(error => {
                resolve({
                    success: false,
                    error: {
                        code: 500,
                        message: error,
                    },
                });
            });
        });
    }

}