import {Storage} from '../storage';

export default (socket, emitter) => {

    socket.on('connect', function() {
        emitter.login();
    });

    socket.on('auth', data => {
        console.info(data);
        if (data && data.coordinates) {
            Storage.dispatch({type: '@profile/CHANGE_COORDINATES', payload: data.coordinates});
        }
    });

    socket.on('disconnect', () => {
        console.info('Disconnected!');
    });

    socket.on('msg', function(data) {
        console.info(data);
    });

    socket.on('errors', function(data) {
        console.info(data);
    });

    socket.on('newOrderInfo', function(data) {
        console.info(data);
        Storage.dispatch({type: '@proposal/RECEIVE', payload: data});
        setTimeout(() => {
            Storage.dispatch({type: '@proposal/REMOVE', payload: data});
        }, 14500);
    });

    return {
        newOrder(subscribe = true) {
            socket[subscribe ? 'on' : 'off']('newOrderInfo', function(data) {
                console.info(data);
            });
        }
    };
}