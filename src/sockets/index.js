import Listeners from './listeners';

export default class Emitter {

    listeners = {};

    constructor(socket, token) {
        this.socket = socket;
        this.token = token;
        this.listeners = Listeners(socket, this);
    }

    login() {
        this.socket.emit('login', {token: this.token});
    }

    sendCoordinates(longitude, latitude) {
        this.socket.emit('submitCoordinates', {
            address: {
                longitude,
                latitude,
            },
        });
    }

    subscribeOnAreas() {
        this.socket.emit('tariffedRegions');
    }

    unSubscribeFromAreas() {
        this.socket.emit('tariffedRegionsUnsubscribe');
    }

}